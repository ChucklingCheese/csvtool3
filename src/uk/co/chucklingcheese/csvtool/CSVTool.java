package uk.co.chucklingcheese.csvtool;

import java.io.BufferedReader;
import java.io.File;
import java.util.Stack;

import uk.co.chucklingcheese.csvtool.asset.Asset;
import uk.co.chucklingcheese.csvtool.asset.AssetUtils;
import uk.co.chucklingcheese.csvtool.csv.CSVOrderParser;
import uk.co.chucklingcheese.csvtool.csv.CSVOrderReformatter;
import uk.co.chucklingcheese.csvtool.csv.CSVParser;
import uk.co.chucklingcheese.csvtool.csv.CSVUtils;
import uk.co.chucklingcheese.csvtool.lang.StringUtils;
import uk.co.chucklingcheese.csvtool.lang.VersionNumber;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 1.0.0
 * @version 3.0.1
 * 
 */
public class CSVTool {
	
	/**
	 * Version number of the application.
	 */
	public static final VersionNumber VERSION_NUMBER = new VersionNumber(3, 0, 1);
	
	/**
	 * Converts all ODS files in the input directory to CSV files.
	 * Currently awaiting implementation...
	 * 
	 * @param odsAssets Stack for ODS files in the input directory.
	 * @param csvAssets Stack for CSV files in the input directory.
	 */
	private static void convertODSFiles(Stack<Asset> odsAssets, Stack<Asset> csvAssets) {
		//System.out.println("\nConverting " + odsAssets.size() + " ODS files...");
	}
	
	/**
	 * Converts all XLSX files in the input directory to CSV files.
	 * Currently awaiting implementation...
	 * 
	 * @param xlsxAssets Stack for XLSX files in the input directory.
	 * @param csvAssets Stack for CSV files in the input directory.
	 */
	private static void convertXLSXFiles(Stack<Asset> xlsxAssets, Stack<Asset> csvAssets) {
		//System.out.println("\nConverting " + xlsxAssets.size() + " XLSX files...");
	}
	
	/**
	 * Processes all CSV files in the input directory.
	 * 
	 * @param assets Stack for CSV files in the input directory.
	 */
	private static void processCSVFiles(Stack<Asset> assets) {
		// Process each of the files in the stack.
		System.out.println("\nProcessing " + assets.size() + " CSV files...");
		for (Asset asset : assets) {
			try (BufferedReader reader = asset.getBufferedReader()) {
				// Find the header line in the file.
				System.out.println("\n" + asset.name.replaceAll(".", "-"));
				System.out.println(asset.name);
				System.out.println(asset.name.replaceAll(".", "-"));
				String lineRead = reader.readLine();
				while(StringUtils.isBlankNotNull(lineRead)) {
					lineRead = reader.readLine();
				}
				if (lineRead == null) {
					System.out.println("> Unreadable contents.");
					reader.close();
					continue;
				}
				
				// Split the headers and read the remaining contents.
				String[] inputHeaders = lineRead.split(CSVUtils.SPLITTING_REGEX);
				Stack<String> linesRead = new Stack<String>();
				while((lineRead = reader.readLine()) != null) {
					linesRead.add(lineRead);
				}
				reader.close();
				
				// Create a parser for the contents.
				System.out.println("> Parsing...");
				CSVParser parser = CSVUtils.getParser(inputHeaders, linesRead);
				if (parser == null) {
					System.out.println("> Unrecognised headers.");
					continue;
				}
				
				// Reformat the file if it contains order exports.
				if (parser instanceof CSVOrderParser) {
					System.out.println("> Reformatting...");
					CSVOrderReformatter.run((CSVOrderParser) parser);
				}
			} catch (Exception exception) {
				// Uh-oh, spaghetti-o!
				System.out.println("> Unreadable contents (fatal).");
				exception.printStackTrace(System.out);
			}
		}
	}
	
	/**
	 * Detects I/O files and processes them accordingly.
	 */
	private static void processFiles() {
		// Erase the existing contents of the output directory.
		// This tool only generates CSV files so ignore everything else.
		File[] files = AssetUtils.OUTPUT_DIRECTORY.listFiles();
		System.out.println("\nDeleting " + files.length + " output files...");
		for (File file : files) {
			if (StringUtils.endsWithIgnoreCase(file.getName(), ".csv")) {
				if (!file.delete()) {
					System.out.println("> Failed to delete: " + file.getAbsolutePath());
					return;
				}
			}
		}
		
		// Stacks for CSV, ODS and XLXS files.
		Stack<Asset> csvAssets = new Stack<Asset>();
		Stack<Asset> odsAssets = new Stack<Asset>();
		Stack<Asset> xlsxAssets = new Stack<Asset>();
		
		// Add the files to their relevant stacks.
		Asset[] assets = AssetUtils.listAssets(AssetUtils.INPUT_DIRECTORY);
		for (Asset asset : assets) {
			if (StringUtils.endsWithIgnoreCase(asset.name, ".csv")) {
				csvAssets.push(asset);
			} else if (StringUtils.endsWithIgnoreCase(asset.name, ".ods")) {
				odsAssets.push(asset);
			} else if (StringUtils.endsWithIgnoreCase(asset.name, ".xlsx")) {
				xlsxAssets.push(asset);
			}
		}
		
		// Convert spreadsheet files first.
		convertODSFiles(odsAssets, csvAssets);
		convertXLSXFiles(xlsxAssets, csvAssets);
		
		// Process all CSV files.
		processCSVFiles(csvAssets);
	}
	
	/**
	 * Launches and initialises the application.
	 * 
	 * @param launchOptions Input received upon launch.
	 */
	public static void main(String[] launchOptions) {
		// Greeting message.
		String greeting = "Chuckling Cheese CSVTool v" + VERSION_NUMBER.toString();
		String greeting2 = "https://bitbucket.org/ChucklingCheese/csvtool";
		System.out.println(String.format("%s\n%s\n%s", greeting, greeting2, greeting2.replaceAll(".", "=")));
		
		// Used to determine time taken to completion.
		long startTime = System.currentTimeMillis();
		
		// Process all input and output files.
		if (AssetUtils.checkDirectories()) {
			processFiles();
		}
		
		// Parting message.
		long ellapsedTime = (System.currentTimeMillis() - startTime);
		String parting = String.format("All valid jobs completed in %sms.", ellapsedTime);
		String parting2 = parting.replaceAll(".", "=");
		System.out.println(String.format("\n%s\n%s\n%s", parting2, parting, parting2));
	}
	
}
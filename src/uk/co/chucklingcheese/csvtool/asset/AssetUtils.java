package uk.co.chucklingcheese.csvtool.asset;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Stack;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 * 
 */
public class AssetUtils {
	
	/**
	 * Directory where input files are stored.
	 */
	public static final File INPUT_DIRECTORY = new File("Input");
	
	/**
	 * Directory where output files are stored.
	 */
	public static final File OUTPUT_DIRECTORY = new File("Output");
	
	/**
	 * Checks if the required directories exist and tries to create them if not.
	 * Should any step of this fail instructions will be provided before exiting.
	 * 
	 * @return Whether both directories exist or were created; true or false.
	 */
	public static boolean checkDirectories() {
		// We can process input files if the directory already exists.
		boolean inputExists = INPUT_DIRECTORY.exists();
		
		// Create the directories.
		File[] directories = { INPUT_DIRECTORY, OUTPUT_DIRECTORY };
		for (File directory : directories) {
			if (!directory.exists()) {
				if (!directory.mkdirs()) {
					System.out.println("\nUnable to create directory: " + directory.getAbsolutePath());
					return false;
				} else {
					System.out.println("\nCreated directory: " + directory.getAbsolutePath());
				}
			}
		}
		return inputExists;
	}
	
	/**
	 * Scans a directory for files and wraps each of them in an asset.
	 * They are all then returned in an array, much like when listing files.
	 * 
	 * @param directory Directory to be scanned for files.
	 * 
	 * @return An array populated with all assets created.
	 */
	public static Asset[] listAssets(File directory) {
		// Scan the directory and all sub-directories for files.
		Stack<File> files = new Stack<File>();
		Stack<File> directories = new Stack<File>();
		directories.add(directory);
		while(!directories.isEmpty()) {
			for (File file : directories.pop().listFiles()) {
				if (file.isFile()) {
					files.add(file);
				} else {
					directories.add(file);
				}
			}
		}
		// Wrap each file in an asset and populate the array.
		Asset[] assets = new Asset[files.size()];
		for (int i = 0; i < assets.length; i++) {
			assets[i] = new FileAsset(files.pop());
		}
		return assets;
	}
	
	/**
	 * Creates a new buffered image from an asset.
	 * 
	 * @param asset Asset containing the image data.
	 * 
	 * @return A buffered image that can be read from, or null.
	 */
	public static BufferedImage getBufferedImage(Asset asset) {
		try {
			return ImageIO.read(asset.getBufferedInputStream());
		} catch (Exception exception) {
			return null;
		}
	}
	
	/**
	 * Creates a new ZIP input stream from an asset.
	 * 
	 * @param asset Asset containing the ZIP file data. 
	 * 
	 * @return A ZIP input stream that can be read from, or null.
	 */
	public static ZipInputStream getZipInputStream(Asset asset) {
		try {
			return new ZipInputStream(asset.getBufferedInputStream());
		} catch (Exception exception) {
			return null;
		}
	}
	
	/**
	 * Creates a new ZIP output stream from an asset.
	 * 
	 * @param asset Asset to contain the ZIP file data.
	 * 
	 * @return A ZIP output stream that can be written to, or null.
	 */
	public static ZipOutputStream getZipOutputStream(Asset asset) {
		try {
			return new ZipOutputStream(asset.getBufferedOutputStream());
		} catch (Exception exception) {
			return null;
		}
	}
	
}
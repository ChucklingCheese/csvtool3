package uk.co.chucklingcheese.csvtool.asset;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 * 
 */
public class JARAsset extends Asset {
	
	/**
	 * Creates a JAR-based asset.
	 * 
	 * @param pathname Pathname of the asset.
	 */
	public JARAsset(String pathname) {
		super("uk/co/chucklingcheese/assets/" + pathname);
	}
	
	/**
	 * Creates a new input stream for reading binary data from the asset.
	 * 
	 * @return An input stream that can be read from, or null.
	 */
	@Override
	public InputStream getInputStream() {
		return ClassLoader.getSystemResourceAsStream(super.pathname);
	}
	
	/**
	 * Creates a new output stream for writing binary data to the asset.
	 * 
	 * @return Always null; these asset types are always read-only.
	 */
	@Override
	public OutputStream getOutputStream() {
		return null;
	}
	
}
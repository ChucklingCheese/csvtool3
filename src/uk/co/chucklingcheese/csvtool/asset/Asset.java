package uk.co.chucklingcheese.csvtool.asset;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 * 
 */
public abstract class Asset {
	
	/**
	 * Name of the asset.
	 */
	public final String name;
	
	/**
	 * Path to the asset.
	 */
	public final String path;
	
	/**
	 * Pathname of the asset.
	 */
	public final String pathname;
	
	/**
	 * Creates a generic asset.
	 * 
	 * @param pathname Pathname of the asset.
	 */
	public Asset(String pathname) {
		// Sanitise the pathname for cross-platform use.
		this.pathname = pathname.replaceAll("\\\\+", "/").replaceAll("//+", File.separator);
		// Determine the path and name of the asset.
		int index = this.pathname.lastIndexOf(File.separator);
		this.path = (index > -1 ? this.pathname.substring(0, index++) : "");
		this.name = (index > -1 ? this.pathname.substring(index, this.pathname.length()) : this.pathname);
	}
	
	/*
	 * These methods determine the basic means by which the asset is accessed.
	 * 
	 * Rather than throwing an exception, a null value should instead be returned to show that their creation was not
	 * successful. This way we only need to check if the object returned is a null value, rather than having to nest
	 * every creation of an asset's I/O objects within a try-catch statement. We will however still need to do this
	 * when performing I/O operations.
	 */
	
	/**
	 * Creates a new input stream for reading binary data from the asset.
	 * 
	 * @return An input stream that can be read from, or null.
	 */
	public abstract InputStream getInputStream();
	
	/**
	 * Creates a new output stream for writing binary data to the asset.
	 * 
	 * @return An output stream that can be written to, or null.
	 */
	public abstract OutputStream getOutputStream();
	
	/**
	 * Creates a new reader for reading character data from the asset.
	 * 
	 * @return A reader that can be read from, or null. 
	 */
	public final Reader getReader() {
		InputStream stream = this.getInputStream();
		try {
			return new InputStreamReader(stream, StandardCharsets.UTF_8);
		} catch (Exception exception) {
			return null;
		}
	}
	
	/**
	 * Creates a new writer for writing character data to the asset.
	 * 
	 * @return A writer that can be written to, or null.
	 */
	public final Writer getWriter() {
		OutputStream stream = this.getOutputStream();
		try {
			return new OutputStreamWriter(stream, StandardCharsets.UTF_8);
		} catch (Exception exception) {
			return null;
		}
	}
	
	/*
	 * These methods create buffered variants of the above which employ a 10Kib buffer to improve the efficiency of
	 * their respective read/write processes. For the same reasons as the unbuffered variants they should each return
	 * a null value rather than throwing an exception. We will however still need to do this when performing I/O
	 * operations.
	 */
	
	/**
	 * Creates a new buffered input stream for reading binary data from the asset.
	 * 
	 * @return A buffered input stream that can be read from, or null.
	 */
	public final BufferedInputStream getBufferedInputStream() {
		InputStream stream = this.getInputStream();
		return (stream == null ? null : new BufferedInputStream(stream, 0x4000));
	}
	
	/**
	 * Creates a new buffered output stream for writing binary data to the asset.
	 * 
	 * @return A buffered output stream that can be written to, or null.
	 */
	public final BufferedOutputStream getBufferedOutputStream() {
		OutputStream stream = this.getOutputStream();
		return (stream == null ? null : new BufferedOutputStream(stream, 0x4000));
	}
	
	/**
	 * Creates a new buffered reader for reading character data from the asset.
	 * 
	 * @return A buffered reader that can be read from, or null.
	 */
	public final BufferedReader getBufferedReader() {
		Reader reader = this.getReader();
		return (reader == null ? null : new BufferedReader(reader, 0x4000));
	}
	
	/**
	 * Creates a new buffered writer for writing character data to the asset.
	 * 
	 * @return A buffered writer that can be written to, or null.
	 */
	public final BufferedWriter getBufferedWriter() {
		Writer writer = this.getWriter();
		return (writer == null ? null : new BufferedWriter(writer, 0x4000));
	}
	
}
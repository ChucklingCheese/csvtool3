package uk.co.chucklingcheese.csvtool.asset;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 * 
 */
public class FileAsset extends Asset {
	
	/**
	 * Whether to append to EOF for new I/O objects.
	 */
	private boolean appending;
	
	/**
	 * Resource for the asset.
	 */
	private final File file;
	
	/**
	 * Creates a file-based asset.
	 * 
	 * @param pathname Pathname of the asset.
	 */
	public FileAsset(String pathname) {
		super(pathname);
		this.appending = false;
		this.file = new File(super.pathname);
	}
	
	/**
	 * Creates a file-based asset.
	 * 
	 * @param file Resource for the asset.
	 */
	public FileAsset(File file) {
		super(file.getAbsolutePath());
		this.appending = false;
		this.file = file;
	}
	
	/**
	 * Enables appending to EOF for new I/O objects.
	 */
	public final void enableAppending() {
		this.appending = true;
	}
	
	/**
	 * Checks whether the assets exists.
	 * 
	 * @return Result of the check; true or false.
	 */
	public final boolean exists() {
		return this.file.exists();
	}
	
	/**
	 * Disables appending to EOF for new I/O objects.
	 */
	public final void disableAppending() {
		this.appending = false;
	}
	
	/**
	 * Creates a new input stream for reading binary data from the asset.
	 * 
	 * @return An input stream that can be read from, or null.
	 */
	@Override
	public InputStream getInputStream() {
		try {
			return new FileInputStream(this.file);
		} catch (Exception exception) {
			return null;
		}
	}
	
	/**
	 * Creates a new output stream for writing binary data to the asset.
	 * 
	 * @return An output stream that can be written to, or null.
	 */
	@Override
	public OutputStream getOutputStream() {
		try {
			File parent = this.file.getParentFile();
			if (parent.exists() || parent.mkdirs()) {
				return new FileOutputStream(this.file, this.appending);
			}
			return null;
		} catch (Exception exception) {
			return null;
		}
	}
	
}
package uk.co.chucklingcheese.csvtool.lang;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 * 
 */
public class VersionNumber {
	
	/**
	 * Major part of the number.
	 */
	public final int major;
	
	/**
	 * Minor part of the number.
	 */
	public final int minor;
	
	/**
	 * Patch part of the number.
	 */
	public final int patch;
	
	/**
	 * Creates a version number.
	 * 
	 * @param major Major part of the number.
	 * @param minor Minor part of the number.
	 * @param patch Patch part of the number.
	 */
	public VersionNumber(int major, int minor, int patch) {
		this.major = major;
		this.minor = minor;
		this.patch = patch;
	}
	
	/**
	 * Represents the version number as a string.
	 * 
	 * @return Version number in MAJOR.MINOR.PATCH layout.
	 */
	@Override
	public String toString() {
		return String.format("%s.%s.%s", this.major, this.minor, this.patch);
	}
	
}
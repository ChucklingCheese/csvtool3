package uk.co.chucklingcheese.csvtool.lang;

import java.util.UUID;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 * 
 */
public class StringUtils {
	
	/**
	 * Checks if the value of a string contains a sequence of characters.
	 * Ignores the casing in both the string and the sequence.
	 * 
	 * @param string String to be checked.
	 * @param sequence Sequence to be checked for.
	 * 
	 * @return Result of the check; true or false.
	 */
	public static boolean containsIgnoreCase(String string, String sequence) {
		if (string == null || sequence == null) {
			return false;
		}
		return string.toLowerCase().contains(sequence.toLowerCase());
	}
	
	/**
	 * Checks if the value of a string ends with a suffix.
	 * Ignores the casing in both the string and the suffix.
	 * 
	 * @param string String to be checked.
	 * @param suffix Suffix to be checked for.
	 * 
	 * @return Result of the check; true or false.
	 */
	public static boolean endsWithIgnoreCase(String string, String suffix) {
		if (string == null || suffix == null) {
			return false;
		}
		return string.toLowerCase().endsWith(suffix.toLowerCase());
	}
	
	/**
	 * Represents a random UUID as a string.
	 * 
	 * @return The UUID as a string.
	 */
	public static String getRandomUUID() {
		return UUID.randomUUID().toString().toUpperCase();
	}
	
	/**
	 * Checks if a string is blank or null.
	 * 
	 * @param string String to be checked.
	 * 
	 * @return Result of the check; true or false.
	 */
	public static boolean isBlankOrNull(String string) {
		return string == null || string.isBlank();
	}
	
	/**
	 * Checks if a string is blank but not null.
	 * 
	 * @param string String to be checked.
	 * 
	 * @return Result of the check; true or false.
	 */
	public static boolean isBlankNotNull(String string) {
		return string != null && string.isBlank();
	}
	
	/**
	 * Checks if a string is neither blank or null.
	 * 
	 * @param string String to be checked.
	 * 
	 * @return Result of the check; true or false.
	 */
	public static boolean isNotBlankNotNull(String string) {
		return !isBlankOrNull(string);
	}
	
	/**
	 * Checks if the value of a string starts with a prefix.
	 * Ignores the casing in both the string and the prefix.
	 * 
	 * @param string String to be checked.
	 * @param prefix Prefix to be checked for.
	 * 
	 * @return Result of the check; true or false.
	 */
	public static boolean startsWithIgnoreCase(String string, String prefix) {
		if (string == null || prefix == null) {
			return false;
		}
		return string.toLowerCase().startsWith(prefix.toLowerCase());
	}
	
	/**
	 * Converts the contents of a string to camel-case.
	 * 
	 * @param string String to convert.
	 * 
	 * @return Result of the conversion.
	 */
	public static String toCamelCase(String string) {
		StringBuilder builder = new StringBuilder();
		String[] words = string.toLowerCase().split("\\s+");
		for (int i = 0; i < words.length; i++) {
			String word = words[i];
			if (!word.isBlank()) {
				builder.append(Character.toUpperCase(word.charAt(0)));
				if (word.length() > 1) {
					builder.append(word.substring(1));
				}
			}
			if ((i + 1) != words.length) {
				builder.append(' ');
			}
		}
		return builder.toString();
	}
	
	/**
	 * Parses a string to a float.
	 * 
	 * @param string String to parse.
	 * @param fallback Fallback value to use.
	 * 
	 * @return Value from the string, or the fallback.
	 */
	public static float toFloat(String string, float fallback) {
		try {
			return Float.parseFloat(string);
		} catch (Exception exception) {
			return fallback;
		}
	}
	
	/**
	 * Parses a string to an integer.
	 * 
	 * @param string String to parse.
	 * @param fallback Fallback value to use.
	 * 
	 * @return Value from the string, or the fallback.
	 */
	public static int toInteger(String string, int fallback) {
		try {
			return Integer.parseInt(string);
		} catch (Exception exception) {
			return fallback;
		}
	}
	
	/**
	 * Parses a string to a long integer.
	 * 
	 * @param string String to parse.
	 * @param fallback Fallback value to use.
	 * 
	 * @return Value from the string, or the fallback.
	 */
	public static long toLong(String string, long fallback) {
		try {
			return Long.parseLong(string);
		} catch (Exception exception) {
			return fallback;
		}
	}
	
}
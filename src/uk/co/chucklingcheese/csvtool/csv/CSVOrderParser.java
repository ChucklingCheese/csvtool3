package uk.co.chucklingcheese.csvtool.csv;

import java.util.HashMap;
import java.util.Stack;

import uk.co.chucklingcheese.csvtool.csv.order.ASotHSParser;
import uk.co.chucklingcheese.csvtool.csv.order.TFMParser;
import uk.co.chucklingcheese.csvtool.csv.order.WowcherParser;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 */
public abstract class CSVOrderParser extends CSVParser {
	
	/**
	 * Map of all cached order line counts.
	 */
	private HashMap<String, Integer> cachedOrderLineCounts = new HashMap<String, Integer>();
	
	/**
	 * Map of all cached order totals.
	 */
	private HashMap<String, Float> cachedOrderValues = new HashMap<String, Float>();
	
	/**
	 * Whether this is a generic merchant platform.
	 */
	public final boolean isGeneric;
	
	/**
	 * Creates an order export-oriented CSV parser.
	 * 
	 * @param name Name of the associated merchant platform.
	 * @param inputHeaders Headers retrieved from an input file.
	 * @param linesRead Lines retrieved from an input file.
	 */
	public CSVOrderParser(String name, String[] inputHeaders, Stack<String> linesRead) {
		this(name, inputHeaders, linesRead, false);
	}
	
	/**
	 * Creates an order export-oriented CSV parser.
	 * 
	 * @param name Name of the associated merchant platform.
	 * @param inputHeaders Headers retrieved from an input file.
	 * @param linesRead Lines retrieved from an input file.
	 * @param isGeneric Whether this is a generic merchant platform.
	 */
	public CSVOrderParser(String name, String[] inputHeaders, Stack<String> linesRead, boolean isGeneric) {
		super(name, inputHeaders, linesRead);
		this.isGeneric = isGeneric;
		
		// Calculate and cache values not provided by some order exports.
		// Some things can be skipped depending on the merchant platform.
		boolean skipOrderValue = false, skipLineCount = false;
		
		// Skip caching of order values and line counts where unnecessary.
		if (this instanceof ASotHSParser || this instanceof TFMParser) {
			skipOrderValue = true;
		} else if (this instanceof WowcherParser) {
			skipOrderValue = true;
			skipLineCount = true;
		}
		
		// Caching will require analysing all lines.
		while (!this.isEmpty()) {
			// Retrieve the order reference.
			String reference = this.getOrderReference();
			
			// Calculate and cache the order value.
			if (!skipOrderValue) {
				// Append the delivery fee only once.
				float orderValue = this.cachedOrderValues.getOrDefault(reference, 0f);
				float shippingValue = this.getOrderShippingValue();
				if (orderValue == 0d && shippingValue > 0f) {
					orderValue += shippingValue;
				}
				// Calculate the order line's item value.
				float price = this.getItemValue();
				orderValue += (price * this.getItemQuantity());
				this.cachedOrderValues.put(reference, orderValue);
			}
			
			// Increment the line count.
			if (!skipLineCount) {
				int lineCount = this.cachedOrderLineCounts.getOrDefault(reference, 0);
				this.cachedOrderLineCounts.put(reference, ++lineCount);
			}
			
			// Analyse the next line.
			this.nextLine();
		}
		// Reset the lines.
		this.resetLines();
	}
	
	/**
	 * Retrieves the line count of a particular order.
	 * 
	 * @param reference Order reference for identification.
	 * 
	 * @return Line count of the order if cached, or zero.
	 */
	public final int getCachedOrderLineCount(String reference) {
		return this.cachedOrderLineCounts.getOrDefault(reference, 0);
	}
	
	/**
	 * Retrieves the total value of a particular order.
	 * 
	 * @param reference Order reference for identification.
	 * 
	 * @return Total value of the order if cached, or zero.
	 */
	public final float getCachedOrderValue(String reference) {
		return this.cachedOrderValues.getOrDefault(reference, 0f);
	}
	
	/*
	 * These methods determine how tokens are generated on a per-platform basis.
	 * 
	 * If there is no value to return, a blank string (or negative one where applicable) should be returned rather than
	 * a null value. This is because the order reformatter will use null values to determine that a value is completely
	 * unsuitable for its required purpose.
	 * 
	 * Any platform-specific changes to a tokens value, such as generating the date in the correct format, should be
	 * performed by the parser. All-encompassing changes such as ensuring postcodes are in the correct structure, etc.
	 * should instead be performed within the order reformatter.
	 */
	
	/**
	 * Retrieves the email address for the contact.
	 * 
	 * @return A valid token, or a blank string.
	 */
	public abstract String getContactEmailAddress();
	
	/**
	 * Retrieves the phone number for the contact.
	 * 
	 * @return A valid token, or negative one.
	 */
	public abstract long getContactPhoneNumber();
	
	/**
	 * Retrieves the first delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	public abstract String getDeliveryAddress1();
	
	/**
	 * Retrieves the second delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	public abstract String getDeliveryAddress2();
	
	/**
	 * Retrieves the third delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	public abstract String getDeliveryAddress3();
	
	/**
	 * Retrieves the country delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	public abstract String getDeliveryCountry();
	
	/**
	 * Retrieves the locality delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	public abstract String getDeliveryLocality();
	
	/**
	 * Retrieves the delivery recipient name.
	 * 
	 * @return A valid token, or a blank string.
	 */
	public abstract String getDeliveryName();
	
	/**
	 * Retrieves the postcode delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	public abstract String getDeliveryPostcode();
	
	/**
	 * Retrieves the message for the item.
	 * 
	 * @return A valid token, or a blank string.
	 */
	public abstract String getItemMessage();
	
	/**
	 * Retrieves the quantity for the item.
	 * 
	 * @return A valid token, or negative one.
	 */
	public abstract int getItemQuantity();
	
	/**
	 * Retrieves the SKU for the item.
	 * 
	 * @return A valid token, or a blank string.
	 */
	public abstract String getItemSKU();
	
	/**
	 * Retrieves the value for the item.
	 * 
	 * @return A valid token, or negative one.
	 */
	public abstract float getItemValue();
	
	/**
	 * Retrieves the delivery date for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	public abstract String getOrderDeliveryDate();
	
	/**
	 * Retrieves the line count for the order.
	 * 
	 * @return Aforementioned line count.
	 */
	public abstract int getOrderLineCount();
	
	/**
	 * Retrieves the purchase date for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	public abstract String getOrderPurchaseDate();
	
	/**
	 * Retrieves the shipping value of the order.
	 * 
	 * @return A valid token, or negative one.
	 */
	public abstract float getOrderShippingValue();
	
	/**
	 * Retrieves the reference for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	public abstract String getOrderReference();
	
	/**
	 * Retrieves the value of the order.
	 * 
	 * @return A valid token, or negative one.
	 */
	public abstract float getOrderValue();
	
}
package uk.co.chucklingcheese.csvtool.csv.order;

import java.util.Stack;

import uk.co.chucklingcheese.csvtool.csv.CSVOrderParser;
import uk.co.chucklingcheese.csvtool.lang.StringUtils;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 */
public final class OnBuyParser extends CSVOrderParser {
	
	/**
	 * Headers the parser will be expecting to see.
	 */
	public static final String[] PLATFORM_HEADERS = {
		"Customer",
		"Delivery Address Country",
		"Delivery Address Line 1",
		"Delivery Address Line 2",
		"Delivery Address Line 3",
		"Delivery Address Name",
		"Delivery Address Postcode",
		"Delivery Address Town",
		"Delivery Service",
		"Order Date",
		"Order Number",
		"Product Unit Price",
		"Quantity",
		"SKU",
		"Total Delivery"
	};
	
	/**
	 * Name of the associated merchant platform.
	 */
	public static final String PLATFORM_NAME = "OnBuy";
	
	/**
	 * Creates a OnBuy-oriented CSV parser.
	 * 
	 * @param inputHeaders Headers retrieved from an input file.
	 * @param linesRead Lines retrieved from an input file.
	 */
	public OnBuyParser(String[] inputHeaders, Stack<String> linesRead) {
		super(PLATFORM_NAME, inputHeaders, linesRead);
	}
	
	/**
	 * Retrieves the email address for the contact.
	 * 
	 * @return A blank string; this is unavailable.
	 */
	@Override
	public String getContactEmailAddress() {
		return "";
	}
	
	/**
	 * Retrieves the phone number for the contact.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public long getContactPhoneNumber() {
		String token = this.getToken("Customer");
		int index = token.lastIndexOf(',');
		if (index == -1) {
			return 0l;
		}
		String phone = token.substring(index + 2);
		return StringUtils.toLong(phone.replaceAll("\\D", ""), -1l);
	}
	
	/**
	 * Retrieves the first delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryAddress1() {
		return this.getToken("Delivery Address Line 1");
	}
	
	/**
	 * Retrieves the second delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryAddress2() {
		return this.getToken("Delivery Address Line 2");
	}
	
	/**
	 * Retrieves the third delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryAddress3() {
		return this.getToken("Delivery Address Line 3");
	}
	
	/**
	 * Retrieves the country delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryCountry() {
		return this.getToken("Delivery Address Country");
	}
	
	/**
	 * Retrieves the locality delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryLocality() {
		return this.getToken("Delivery Address Town");
	}
	
	/**
	 * Retrieves the delivery recipient name.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryName() {
		return this.getToken("Delivery Address Name");
	}
	
	/**
	 * Retrieves the postcode delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryPostcode() {
		return this.getToken("Delivery Address Postcode");
	}
	
	/**
	 * Retrieves the message for the item.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getItemMessage() {
		String token = this.getToken("Customer");
		int index = token.lastIndexOf(',');
		if (index == -1) {
			return "";
		}
		String customer = token.substring(0, index);
		return "From " + customer;
	}
	
	/**
	 * Retrieves the quantity for the item.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public int getItemQuantity() {
		return StringUtils.toInteger(this.getToken("Quantity"), -1);
	}
	
	/**
	 * Retrieves the SKU for the item.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getItemSKU() {
		return this.getToken("SKU");
	}
	
	/**
	 * Retrieves the value for the item.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public float getItemValue() {
		return StringUtils.toFloat(this.getToken("Product Unit Price"), -1f);
	}
	
	/**
	 * Retrieves the delivery date for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderDeliveryDate() {
		return this.getOrderPurchaseDate();
	}
	
	/**
	 * Retrieves the line count for the order.
	 * 
	 * @return Aforementioned line count.
	 */
	@Override
	public int getOrderLineCount() {
		return this.getCachedOrderLineCount(this.getOrderReference());
	}
	
	/**
	 * Retrieves the purchase date for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderPurchaseDate() {
		return this.getToken("Order Date");
	}
	
	/**
	 * Retrieves the shipping value of the order.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public float getOrderShippingValue() {
		return StringUtils.toFloat(this.getToken("Total Delivery"), -1f);
	}
	
	/**
	 * Retrieves the reference for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderReference() {
		return this.getToken("Order Number");
	}
	
	/**
	 * Retrieves the value of the order.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public float getOrderValue() {
		return this.getCachedOrderValue(this.getOrderReference());
	}
	
}
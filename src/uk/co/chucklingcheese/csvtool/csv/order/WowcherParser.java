package uk.co.chucklingcheese.csvtool.csv.order;

import java.util.Stack;

import uk.co.chucklingcheese.csvtool.csv.CSVOrderParser;
import uk.co.chucklingcheese.csvtool.lang.StringUtils;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 */
public final class WowcherParser extends CSVOrderParser {
	
	/**
	 * Headers the parser will be expecting to see.
	 */
	public static final String[] PLATFORM_HEADERS = {
		"Address line 1",
		"Address line 2",
		"City",
		"Customer Name",
		"Deal Title",
		"Email",
		"House Number",
		"Phone",
		"Postcode",
		"Redeemed at",
		"SKU",
		"Wowcher Code"
	};
	
	/**
	 * Name of the associated merchant platform.
	 */
	public static final String PLATFORM_NAME = "Wowcher";
	
	/**
	 * Creates a Wowcher-oriented CSV parser.
	 * 
	 * @param inputHeaders Headers retrieved from an input file.
	 * @param linesRead Lines retrieved from an input file.
	 */
	public WowcherParser(String[] inputHeaders, Stack<String> linesRead) {
		super(PLATFORM_NAME, inputHeaders, linesRead);
	}
	
	/**
	 * Retrieves the email address for the contact.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getContactEmailAddress() {
		return this.getToken("Email");
	}
	
	/**
	 * Retrieves the phone number for the contact.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public long getContactPhoneNumber() {
		String phone = this.getToken("Phone");
		return StringUtils.toLong(phone.replaceAll("\\D", ""), 0l);
	}
	
	/**
	 * Retrieves the first delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryAddress1() {
		return this.getToken("Address line 1");
	}
	
	/**
	 * Retrieves the second delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryAddress2() {
		return this.getToken("Address line 2");
	}
	
	/**
	 * Retrieves the third delivery address line.
	 * 
	 * @return Always blank; this is unavailable.
	 */
	@Override
	public String getDeliveryAddress3() {
		return "";
	}
	
	/**
	 * Retrieves the country delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryCountry() {
		// Not exported. We only ship nationally.
		return "United Kingdom";
	}
	
	/**
	 * Retrieves the locality delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryLocality() {
		return this.getToken("City");
	}
	
	/**
	 * Retrieves the delivery recipient name.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryName() {
		return this.getToken("Customer Name");
	}
	
	/**
	 * Retrieves the postcode delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryPostcode() {
		return this.getToken("Postcode");
	}
	
	/**
	 * Retrieves the message for the item.
	 * 
	 * @return Always blank; this is unavailable.
	 */
	@Override
	public String getItemMessage() {
		return "";
	}
	
	/**
	 * Retrieves the quantity for the item.
	 * 
	 * @return Always 1; this is fixed.
	 */
	@Override
	public int getItemQuantity() {
		// Not exported. Deals are always per-unit.
		return 1;
	}
	
	/**
	 * Retrieves the SKU for the item.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getItemSKU() {
		return this.getToken("SKU");
	}
	
	/**
	 * Retrieves the value for the item.
	 * 
	 * @return Always 19; this is fixed.
	 */
	@Override
	public float getItemValue() {
		// Not exported. Does not currently vary.
		return 19f;
	}
	
	/**
	 * Retrieves the delivery date for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderDeliveryDate() {
		return this.getOrderPurchaseDate();
	}
	
	/**
	 * Retrieves the line count for the order.
	 * 
	 * @return Always 1; this is fixed.
	 */
	@Override
	public int getOrderLineCount() {
		// Deals only consist of a single line.
		return 1;
	}
	
	/**
	 * Retrieves the purchase date for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderPurchaseDate() {
		return this.getToken("Redeemed at");
	}
	
	/**
	 * Retrieves the shipping value of the order.
	 * 
	 * @return Always 2.95; this is fixed.
	 */
	@Override
	public float getOrderShippingValue() {
		return 2.95f;
	}
	
	/**
	 * Retrieves the reference for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderReference() {
		return this.getToken("Wowcher Code");
	}
	
	/**
	 * Retrieves the value of the order.
	 * 
	 * @return Always 21.95; this is fixed.
	 */
	@Override
	public float getOrderValue() {
		return this.getItemValue() + this.getOrderShippingValue();
	}
	
}
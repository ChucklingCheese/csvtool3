package uk.co.chucklingcheese.csvtool.csv.order;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Stack;

import uk.co.chucklingcheese.csvtool.csv.CSVOrderParser;
import uk.co.chucklingcheese.csvtool.lang.StringUtils;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 */
public final class EtsyParser extends CSVOrderParser {
	
	/**
	 * Headers the parser will be expecting to see.
	 */
	public static final String[] PLATFORM_HEADERS = {
		"Buyer",
		"Currency",
		"Delivery Address1",
		"Delivery Address2",
		"Delivery City",
		"Delivery Country",
		"Delivery Name",
		"Delivery Zipcode",
		"Order Delivery",
		"Order ID",
		"Price",
		"Quantity",
		"Sale Date",
		"SKU"
	};
	
	/**
	 * Name of the associated merchant platform.
	 */
	public static final String PLATFORM_NAME = "Etsy";
	
	/**
	 * Creates a Etsy-oriented CSV parser.
	 * 
	 * @param inputHeaders Headers retrieved from an input file.
	 * @param linesRead Lines retrieved from an input file.
	 */
	public EtsyParser(String[] inputHeaders, Stack<String> linesRead) {
		super(PLATFORM_NAME, inputHeaders, linesRead);
	}
	
	/**
	 * Retrieves the email address for the contact.
	 * 
	 * @return A blank string; this is unavailable
	 */
	@Override
	public String getContactEmailAddress() {
		return "";
	}
	
	/**
	 * Retrieves the phone number for the contact.
	 * 
	 * @return Always zero; this is unavailable.
	 */
	@Override
	public long getContactPhoneNumber() {
		return 0;
	}
	
	/**
	 * Retrieves the first delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryAddress1() {
		return this.getToken("Delivery Address1");
	}
	
	/**
	 * Retrieves the second delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryAddress2() {
		return this.getToken("Delivery Address2");
	}
	
	/**
	 * Retrieves the third delivery address line.
	 * 
	 * @return Always blank; this is unavailable.
	 */
	@Override
	public String getDeliveryAddress3() {
		return "";
	}
	
	/**
	 * Retrieves the country delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryCountry() {
		return this.getToken("Delivery Country");
	}
	
	/**
	 * Retrieves the locality delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryLocality() {
		return this.getToken("Delivery City");
	}
	
	/**
	 * Retrieves the delivery recipient name.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryName() {
		return this.getToken("Delivery Name");
	}
	
	/**
	 * Retrieves the postcode delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryPostcode() {
		return this.getToken("Delivery Zipcode");
	}
	
	/**
	 * Retrieves the message for the item.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getItemMessage() {
		return "From " + this.getToken("Buyer");
	}
	
	/**
	 * Retrieves the quantity for the item.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public int getItemQuantity() {
		return StringUtils.toInteger(this.getToken("Quantity"), -1);
	}
	
	/**
	 * Retrieves the SKU for the item.
	 * 
	 * @return A valid token, or a blank string..
	 */
	@Override
	public String getItemSKU() {
		return this.getToken("SKU");
	}
	
	/**
	 * Retrieves the value for the item.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public float getItemValue() {
		return StringUtils.toFloat(this.getToken("Price"), -1f);
	}
	
	/**
	 * Retrieves the delivery date for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderDeliveryDate() {
		return this.getOrderPurchaseDate();
	}
	
	/**
	 * Retrieves the line count for the order.
	 * 
	 * @return Aforementioned line count.
	 */
	@Override
	public int getOrderLineCount() {
		return this.getCachedOrderLineCount(this.getOrderReference());
	}
	
	/**
	 * Retrieves the purchase date for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderPurchaseDate() {
		try {
			// Create a calendar set to the date.
			String date = this.getToken("Sale Date");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new SimpleDateFormat("MM-dd-yy").parse(date));
			
			// Retrieve each of the date components.
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			int month = calendar.get(Calendar.MONTH);
			int year = calendar.get(Calendar.YEAR);
			
			// Make some slight changes required by the importer.
			String day2 = (day < 10 ? ("0" + day) : Integer.toString(day));
			String month2 = (++month < 10 ? ("0" + month) : Integer.toString(month));
			
			// Return the date in the format required.
			return String.format("%s-%s-%s 00:00:00", year, month2, day2);
		} catch (Exception exception) {
			// Return today's date and time in the format required.
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
		}
	}
	
	/**
	 * Retrieves the shipping value of the order.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public float getOrderShippingValue() {
		return StringUtils.toFloat(this.getToken("Order Delivery"), -1f);
	}
	
	/**
	 * Retrieves the reference for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderReference() {
		return this.getToken("Order ID");
	}
	
	/**
	 * Retrieves the value of the order.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public float getOrderValue() {
		return this.getCachedOrderValue(this.getOrderReference());
	}
	
}
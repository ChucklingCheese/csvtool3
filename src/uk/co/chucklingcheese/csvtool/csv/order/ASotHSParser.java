package uk.co.chucklingcheese.csvtool.csv.order;

import java.util.Stack;

import uk.co.chucklingcheese.csvtool.csv.CSVOrderParser;
import uk.co.chucklingcheese.csvtool.lang.StringUtils;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 */
public final class ASotHSParser extends CSVOrderParser {
	
	/**
	 * Headers the parser will be expecting to see.
	 */
	public static final String[] PLATFORM_HEADERS = {
			"Address1",
			"Address2",
			"Amount Paid By Buyer",
			"Buyer's Contact Number",
			"Buyer's Email Address",
			"City",
			"Country",
			"First Name",
			"Last Name",
			"Order#",
			"Order Date (Europe/London)",
			"Order Note",
			"Product Price",
			"Quantity Ordered",
			"Shipping Address",
			"Shipping Paid By Buyer",
			"SKU",
			"Zipcode"
	};
	
	/**
	 * Name of the associated merchant platform.
	 */
	public static final String PLATFORM_NAME = "ASotHS";
	
	/**
	 * Creates an As Seen on the High Street-oriented CSV parser.
	 * 
	 * @param inputHeaders Headers retrieved from an input file.
	 * @param linesRead Lines retrieved from an input file.
	 */
	public ASotHSParser(String[] inputHeaders, Stack<String> linesRead) {
		super(PLATFORM_NAME, inputHeaders, linesRead);
	}
	
	/**
	 * Retrieves the email address for the contact.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getContactEmailAddress() {
		return this.getToken("Buyer's Email Address");
	}
	
	/**
	 * Retrieves the phone number for the contact.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public long getContactPhoneNumber() {
		String phone = this.getToken("Buyer's Contact Number");
		return StringUtils.toLong(phone.replaceAll("\\D", ""), -1l);
	}
	
	/**
	 * Retrieves the first delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryAddress1() {
		return this.getToken("Address1");
	}
	
	/**
	 * Retrieves the second delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryAddress2() {
		return this.getToken("Address2");
	}
	
	/**
	 * Retrieves the third delivery address line.
	 * 
	 * @return Always blank; this is unavailable.
	 */
	@Override
	public String getDeliveryAddress3() {
		return "";
	}
	
	/**
	 * Retrieves the country delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryCountry() {
		return this.getToken("Country");
	}
	
	/**
	 * Retrieves the locality delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryLocality() {
		return this.getToken("City");
	}
	
	/**
	 * Retrieves the delivery recipient name.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryName() {
		return this.getToken("Shipping Address").split("\n", -1)[0];
	}
	
	/**
	 * Retrieves the postcode delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryPostcode() {
		return this.getToken("Zipcode");
	}
	
	/**
	 * Retrieves the message for the item.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getItemMessage() {
		String message = this.getToken("Order Note");
		if (!message.isBlank()) {
			return message;
		}
		// This is the billing name.
		String firstName = this.getToken("First Name");
		String lastName = this.getToken("Last Name");
		return String.format("From %s %s", firstName, lastName);
	}
	
	/**
	 * Retrieves the quantity for the item.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public int getItemQuantity() {
		return StringUtils.toInteger(this.getToken("Quantity Ordered"), -1);
	}
	
	/**
	 * Retrieves the SKU for the item.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getItemSKU() {
		String sku = this.getToken("SKU");
		return sku.equals("N/A") ? "" : sku;
	}
	
	/**
	 * Retrieves the value for the item.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public float getItemValue() {
		return StringUtils.toFloat(this.getToken("Product Price"), -1f);
	}
	
	/**
	 * Retrieves the delivery date for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderDeliveryDate() {
		return this.getOrderPurchaseDate();
	}
	
	/**
	 * Retrieves the line count for the order.
	 * 
	 * @return Aforementioned line count.
	 */
	@Override
	public int getOrderLineCount() {
		return this.getCachedOrderLineCount(this.getOrderReference());
	}
	
	/**
	 * Retrieves the purchase date for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderPurchaseDate() {
		return this.getToken("Order Date (Europe/London)");
	}
	
	/**
	 * Retrieves the shipping value of the order.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public float getOrderShippingValue() {
		return StringUtils.toFloat(this.getToken("Shipping Paid By Buyer"), -1f);
	}
	
	/**
	 * Retrieves the reference for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderReference() {
		return this.getToken("Order#");
	}
	
	/**
	 * Retrieves the value of the order.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public float getOrderValue() {
		return StringUtils.toFloat(this.getToken("Amount Paid By Buyer"), -1f);
	}
	
}
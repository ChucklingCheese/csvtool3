package uk.co.chucklingcheese.csvtool.csv.order;

import java.util.Stack;

import uk.co.chucklingcheese.csvtool.csv.CSVOrderParser;
import uk.co.chucklingcheese.csvtool.lang.StringUtils;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 */
public final class UDSParser extends CSVOrderParser {
	
	/**
	 * Headers the parser will be expecting to see.
	 */
	public static final String[] PLATFORM_HEADERS = {
		"bill_company",
		"bill_firstname",
		"bill_lastname",
		"created_at",
		"message",
		"order_id",
		"price",
		"quantity",
		"ship_address_1",
		"ship_city",
		"ship_company",
		"ship_country",
		"ship_firstname",
		"ship_lastname",
		"ship_postcode",
		"ship_telephone",
		"shipping_amount",
		"sku"
	};
	
	/**
	 * Name of the associated merchant platform.
	 */
	public static final String PLATFORM_NAME = "uDropShip";
	
	/**
	 * Creates a uDropShip-oriented CSV parser.
	 * 
	 * @param inputHeaders Headers retrieved from an input file.
	 * @param linesRead Lines retrieved from an input file.
	 */
	public UDSParser(String[] inputHeaders, Stack<String> linesRead) {
		super(PLATFORM_NAME, inputHeaders, linesRead, true);
	}
	
	/**
	 * Retrieves the email address for the contact.
	 * 
	 * @return A blank string; this is unavailable.
	 */
	@Override
	public String getContactEmailAddress() {
		return "";
	}
	
	/**
	 * Retrieves the phone number for the contact.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public long getContactPhoneNumber() {
		String phone = this.getToken("ship_telephone");
		return StringUtils.toLong(phone.replaceAll("\\D", ""), 0l);
	}
	
	/**
	 * Retrieves the first delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryAddress1() {
		String company = this.getToken("ship_company");
		if (!company.isBlank()) {
			return company;
		}
		return this.getToken("ship_address_1").split("\n", -1)[0];
	}
	
	/**
	 * Retrieves the second delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryAddress2() {
		String company = this.getToken("ship_company");
		String[] lines = this.getToken("ship_address_1").split("\n", -1);
		if (!company.isBlank()) {
			return lines[0];
		}
		return lines.length > 1 ? lines[1] : "";
	}
	
	/**
	 * Retrieves the third delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryAddress3() {
		String company = this.getToken("ship_company");
		String[] lines = this.getToken("ship_address_1").split("\n", -1);
		if (!company.isBlank()) {
			return lines.length > 1 ? lines[1] : "";
		}
		return "";
	}
	
	/**
	 * Retrieves the country delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryCountry() {
		// To-do: Match country codes to country names.
		String code = this.getToken("ship_country");
		if (code.equals("GB")) {
			return "United Kingdom";
		}
		return "";
	}
	
	/**
	 * Retrieves the locality delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryLocality() {
		return this.getToken("ship_city");
	}
	
	/**
	 * Retrieves the delivery recipient name.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryName() {
		String firstName = this.getToken("ship_firstname");
		String lastName = this.getToken("ship_lastname");
		return String.format("%s %s", firstName, lastName);
	}
	
	/**
	 * Retrieves the postcode delivery address line.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getDeliveryPostcode() {
		return this.getToken("ship_postcode");
	}
	
	/**
	 * Retrieves the message for the item.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getItemMessage() {
		String message = this.getToken("message");
		if (!message.isBlank()) {
			return message;
		}
		String compName = this.getToken("bill_company");
		String firstName = this.getToken("bill_firstname");
		String lastName = this.getToken("bill_lastname");
		if (!compName.isBlank()) {
			return String.format("From %s %s at %s", firstName, lastName, compName);
		}
		return String.format("From %s %s", firstName, lastName);
	}
	
	/**
	 * Retrieves the quantity for the item.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public int getItemQuantity() {
		// Remove all unnecessary decimals.
		String quantity = this.getToken("quantity");
		return StringUtils.toInteger(quantity.substring(0, quantity.indexOf('.')), -1);
	}
	
	/**
	 * Retrieves the SKU for the item.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getItemSKU() {
		// Remove the seller ID prefix.
		String sku = this.getToken("sku");
		return sku.substring(sku.indexOf('-') + 1);
	}
	
	/**
	 * Retrieves the value for the item.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public float getItemValue() {
		return StringUtils.toFloat(this.getToken("price"), -1f);
	}
	
	/**
	 * Retrieves the delivery date for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderDeliveryDate() {
		return this.getOrderPurchaseDate();
	}
	
	/**
	 * Retrieves the line count for the order.
	 * 
	 * @return Aforementioned line count.
	 */
	@Override
	public int getOrderLineCount() {
		return this.getCachedOrderLineCount(this.getOrderReference());
	}
	
	/**
	 * Retrieves the purchase date for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderPurchaseDate() {
		return this.getToken("created_at");
	}
	
	/**
	 * Retrieves the shipping value of the order.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public float getOrderShippingValue() {
		return StringUtils.toFloat(this.getToken("shipping_amount"), -1f);
	}
	
	/**
	 * Retrieves the reference for the order.
	 * 
	 * @return A valid token, or a blank string.
	 */
	@Override
	public String getOrderReference() {
		return this.getToken("order_id");
	}
	
	/**
	 * Retrieves the value of the order.
	 * 
	 * @return A valid token, or negative one.
	 */
	@Override
	public float getOrderValue() {
		return this.getCachedOrderValue(this.getOrderReference());
	}
	
}
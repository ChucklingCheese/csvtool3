package uk.co.chucklingcheese.csvtool.csv;

import java.io.BufferedWriter;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Stack;

import uk.co.chucklingcheese.csvtool.asset.AssetUtils;
import uk.co.chucklingcheese.csvtool.asset.FileAsset;
import uk.co.chucklingcheese.csvtool.lang.StringUtils;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 */
public final class CSVOrderReformatter {
	
	/**
	 * Headers the order reformatter will be expecting to use.
	 */
	private static final String[] OUTPUT_HEADERS = {
			"Order Purchase Date",
			"Order Value",
			"Promotions",
			"Order Reference",
			"Unused (E)",
			"SKU",
			"Unused (G)",
			"Quantity",
			"Item Value",
			"Unused (J)",
			"Order Shipping Value",
			"Unused (L)",
			"Unused (M)",
			"Delivery Name",
			"Delivery Address 1",
			"Delivery Address 2",
			"Delivery Address 3",
			"Delivery Locality",
			"Unused (S)",
			"Delivery Postcode",
			"Delivery Country",
			"Contact Phone Number",
			"Unused (W)",
			"Unused (X)",
			"Order Delivery Date",
			"Unused (Z)",
			"Unused (AA)",
			"Unused (AB)",
			"Item Message",
			"Unused (AD)",
			"Contact Email Address"
	};
	
	/**
	 * Reformats the contents of an order parser.
	 * All checks beyond basic data presentation are performed here.
	 * 
	 * @param parser Order parser containing contents to be reformatted.
	 */
	public static void run(CSVOrderParser parser) {
		// Stack the reformatted lines so they can be written at once.
		StringBuilder lineBuilder = new StringBuilder();
		Stack<String> linesBuilt = new Stack<String>();
		
		// Parsers for generic platforms will need to avoid being merged together.
		// A UUID is generated to make each file unique from others of the same platform.
		FileAsset asset = null;
		if (parser.isGeneric) {
			String name = String.format("%s %s.csv", parser.name, StringUtils.getRandomUUID());
			asset = new FileAsset(new File(AssetUtils.OUTPUT_DIRECTORY, name));
		} else {
			String name = String.format("%s.csv", parser.name);
			asset = new FileAsset(new File(AssetUtils.OUTPUT_DIRECTORY, name));
		}
		
		// Write the output headers first if this file is being created.
		if (!asset.exists()) {
			for (int i = 0; i < OUTPUT_HEADERS.length; i++) {
				lineBuilder.append(String.format("\"%s\"", OUTPUT_HEADERS[i]));
				if ((i + 1) != OUTPUT_HEADERS.length) {
					lineBuilder.append(',');
				}
			}
			linesBuilt.push(lineBuilder.toString());
		}
		
		// Otherwise the contents can just be appended to the end of the existing file.
		// This avoids having orders from the same origin becoming split across multiple files.
		else {
			asset.enableAppending();
		}
		
		// Loop through every line that was parsed.
		main: while(!parser.isEmpty()) {
			// Build a token for each required output header.
			lineBuilder = new StringBuilder();
			DecimalFormat decimalFormat = new DecimalFormat("##.##");
			for (int i = 0; i < OUTPUT_HEADERS.length; i++) {
				// This should remain null where the value is invalid.
				// Doing so will trigger an error report for the header.
				String value = null;
				
				// A: Order Purchase Date
				if (i == 0) {
					String orderPurchaseDate = parser.getOrderPurchaseDate();
					if (StringUtils.isNotBlankNotNull(orderPurchaseDate)) {
						if (orderPurchaseDate.length() >= 19) {
							// The 'T' character may be present to separate date from time.
							value = orderPurchaseDate.substring(0, 19).replace('T', ' ');
						}
					}
				}
				
				// B: Order Value
				else if (i == 1) {
					float orderValue = parser.getOrderValue();
					if (orderValue > 0f) {
						value = decimalFormat.format(orderValue);
					}
				}
				
				// C: Promotions
				else if (i == 2) {
					value = "0";
				}
				
				// D: Order Reference
				else if (i == 3) {
					String orderReference = parser.getOrderReference();
					if (StringUtils.isNotBlankNotNull(orderReference)) {
						value = orderReference;
					}
				}
				
				// F: SKU
				else if (i == 5) {
					String sku = parser.getItemSKU();
					if (StringUtils.isNotBlankNotNull(sku)) {
						value = sku;
					}
				}
				
				// H: Quantity
				else if (i == 7) {
					int quantity = parser.getItemQuantity();
					if (quantity > 0) {
						value = Integer.toString(quantity);
					}
				}
				
				// I: Item Value
				else if (i == 8) {
					float itemValue = parser.getItemValue();
					if (itemValue > 0f) {
						value = decimalFormat.format(itemValue);
					}
				}
				
				// K: Order Shipping Value
				else if (i == 10) {
					// Shipping won't always be charged.
					float orderShippingValue = parser.getOrderShippingValue();
					if (orderShippingValue >= 0f) {
						value = decimalFormat.format(orderShippingValue);
					}
				}
				
				// N: Delivery Name
				else if (i ==  13) {
					String deliveryName = parser.getDeliveryName();
					if (StringUtils.isNotBlankNotNull(deliveryName)) {
						value = StringUtils.toCamelCase(deliveryName);
					}
				}
				
				// O: Delivery Address 1
				else if (i ==  14) {
					String deliveryAddress1 = parser.getDeliveryAddress1();
					if (StringUtils.isNotBlankNotNull(deliveryAddress1)) {
						value = StringUtils.toCamelCase(deliveryAddress1);
					}
				}
				
				// P: Delivery Address 2
				else if (i ==  15) {
					String deliveryAddress2 = parser.getDeliveryAddress2();
					if (StringUtils.isNotBlankNotNull(deliveryAddress2)) {
						value = StringUtils.toCamelCase(deliveryAddress2);
					// Non-essential.
					} else {
						value = "";
					}
				}
				
				// Q: Delivery Address 3
				else if (i ==  16) {
					String deliveryAddress3 = parser.getDeliveryAddress3();
					if (StringUtils.isNotBlankNotNull(deliveryAddress3)) {
						value = StringUtils.toCamelCase(deliveryAddress3);
					// Non-essential.
					} else {
						value = "";
					}
				}
				
				// R: Delivery Locality
				else if (i ==  17) {
					String deliveryLocality = parser.getDeliveryLocality();
					if (StringUtils.isNotBlankNotNull(deliveryLocality)) {
						value = deliveryLocality.toUpperCase();
					}
				}
				
				// T: Delivery Postcode
				else if (i == 19) {
					String postcode = parser.getDeliveryPostcode();
					if (StringUtils.isNotBlankNotNull(postcode)) {
						postcode = postcode.toUpperCase().replaceAll("[\\W]", "");
						if (postcode.length() >= 5 && postcode.length() <= 8) {
							int index = postcode.length() - 3;
							String innerPostcode = postcode.substring(0, index);
							String outerPostcode = postcode.substring(index);
							value = String.format("%s %s", innerPostcode, outerPostcode);
						}
					}
				}
				
				// U: Delivery Country
				else if (i == 20) {
					String deliveryCountry = parser.getDeliveryCountry();
					if (StringUtils.isNotBlankNotNull(deliveryCountry)) {
						value = StringUtils.toCamelCase(deliveryCountry);
					}
				}
				
				// V: Contact Phone Number
				else if (i == 21) {
					// National phone numbers with the country code.
					long phone = parser.getContactPhoneNumber();
					if (phone >= 0x66720B3000l && phone <= 0x68C61713FFl) {
						phone -= 0x66720B3000l;
					}
					// National phone numbers without the country code. 
					if (phone >= 0x3B9ACA00l && phone <= 0x2540BE3FFl) {
						// 04 numbers are reserved.
						if (phone >= 0xEE6B2800l && phone <= 0x12A05F1FFl) {
							// Non-essential.
							value = "";
						}
						// 06 numbers are reserved.
						else if (phone >= 0x165A0BC00l && phone <= 0x1A13B85FFl) {
							// Non-essential.
							value = "";
						}
						// Reintroduce the country code to the phone number.
						// This prevents spreadsheet software removing the zero.
						else {
							value = Long.toString(phone + 0x66720B3000l);
						}
					// International phone numbers.
					} else {
						// Non-essential.
						value = "";
					}
				}
				
				// Y: Order Delivery Date
				else if (i == 24) {
					String orderDeliveryDate = parser.getOrderDeliveryDate();
					if (StringUtils.isNotBlankNotNull(orderDeliveryDate)) {
						if (orderDeliveryDate.length() >= 10) {
							// The 'T' character may be present to separate date from time.
							value = orderDeliveryDate.substring(0, 10).replace('T', ' ');
						}
					}
				}
				
				// AC: Item Message
				else if (i == 28) {
					String itemMessage = parser.getItemMessage();
					if (StringUtils.isNotBlankNotNull(itemMessage)) {
						if (itemMessage.length() > 250) {
							itemMessage = itemMessage.substring(0, 247) + "...";
						}
						value = itemMessage;
					// Non-essential.
					} else {
						value = "";
					}
				}
				
				// AE: Contact Email Address
				else if (i == 30) {
					String contactEmailAddress = parser.getContactEmailAddress();
					if (StringUtils.isNotBlankNotNull(contactEmailAddress)) {
						value = parser.getContactEmailAddress().toLowerCase();
					// Non-essential.
					} else {
						value = "";
					}
				}
				
				// Unused headers.
				else {
					value = "";
				}
				
				// Report any errors with the value.
				if (value == null) {
					String reference = parser.getOrderReference();
					if (StringUtils.isBlankOrNull(reference)) {
						System.out.println("  > Order Reference token invalid.");
					} else {
						System.out.println("  > " + OUTPUT_HEADERS[i] + " token invalid for " + reference);
					}
					parser.nextLine();
					continue main;
				}
				
				// Append the value to the order line.
				lineBuilder.append(String.format("\"%s\"", value != null ? value.strip() : ""));
				if ((i + 1) != OUTPUT_HEADERS.length) {
					lineBuilder.append(',');
				}
			}
			
			// Reformat the line and begin the next.
			linesBuilt.add(lineBuilder.toString());
			parser.nextLine();
		}
		
		// Write the reformatted contents to the output file.
		try (BufferedWriter writer = asset.getBufferedWriter()) {
			for (String lineBuilt : linesBuilt) {
				writer.write(lineBuilt);
				writer.newLine();
			}
			writer.close();
		} catch (Exception exception) {
			// Uh-oh, spaghetti-o.
			System.out.println("> Unwritable contents (fatal).");
			exception.printStackTrace();
			return;
		}
		
		// Woo!
		System.out.println("> Finished!");
	}
	
}
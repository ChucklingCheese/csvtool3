package uk.co.chucklingcheese.csvtool.csv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import uk.co.chucklingcheese.csvtool.csv.order.BBoxParser;
import uk.co.chucklingcheese.csvtool.csv.order.EbayParser;
import uk.co.chucklingcheese.csvtool.csv.order.UDSParser;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 */
public class CSVParser {
	
	/**
	 * Index of the current line and tokens.
	 */
	private int currentIndex = 0;
	
	/**
	 * Current line being accessed.
	 */
	private String currentLine;
	
	/**
	 * Current tokens being accessed.
	 */
	private String[] currentTokens;
	
	/**
	 * List of all lines read from the file.
	 */
	private List<String> linesRead = new ArrayList<String>();
	
	/**
	 * Map of each header name to a numerical value.
	 */
	private HashMap<String, Integer> headerIndices = new HashMap<String, Integer>();
	
	/**
	 * Name of the associated merchant platform.
	 */
	public final String name;
	
	/**
	 * Creates a generic CSV parser.
	 * 
	 * @param name Name to associate with the parser.
	 * @param inputHeaders Headers retrieved from an input file.
	 * @param linesRead Lines retrieved from an input file.
	 */
	public CSVParser(String name, String[] inputHeaders, Stack<String> linesRead) {
		// Some information read from the input file may need to be ignored.
		// See the comment blocks where these are used for additional details.
		int headerOffsetCount = 0, lineSkipCount = 0;
		
		// Due do the requirements of classes inheriting from this, some checks need to be made here.
		// Without them it would not be possible to read certain CSV files due to their differences.
		
		// Map each of the input headers to their positions within the array.
		for (int i = 0; i < inputHeaders.length; i++) {
			// uDropship input headers may contain several custom options that will need to be ignored.
			// We are unable to use this feature in any meaningful way due to the design of our importer.
			// That no tokens are included for them when unused would also cause synchronisation issues.
			if (this instanceof UDSParser && inputHeaders[i].startsWith("custom_option_")) {
				headerOffsetCount++;
				continue;
			}
			this.headerIndices.putIfAbsent(inputHeaders[i].replaceAll("\"", ""), i);
		}
		
		// The first (originally last) few lines in some order exports contain no relevant information.
		// Unless these are skipped they will cause issues later when empty tokens begin to be encountered.
		lineSkipCount = this instanceof BBoxParser ? 1 : lineSkipCount;
		lineSkipCount = this instanceof EbayParser ? 2 : lineSkipCount;
		for (int i = 0; i < lineSkipCount; i++) {
			if (linesRead.size() >= lineSkipCount) {
				linesRead.pop();
			}
		}
		
		// Line validation and reconstruction primarily consists of merging and error reporting.
		// Breaks will simply prevent further lines from being read; the parser will remain operable.
		main: while(!linesRead.isEmpty()) {
			// Read the next line and split up its tokens.
			String lineRead = linesRead.pop().replaceAll("\\s+", " ");
			String[] lineTokens = lineRead.split(CSVUtils.SPLITTING_REGEX, -1);
			
			// Ignore the line if it contains only empty tokens.
			if (lineRead.replaceAll(",\"\"", "").length() <= 1) {
				continue;
			}
			
			// Gift messages may cause some order lines to become split up across multiple lines in the stack.
			// Additional tokens will continuously be read until their length matches that of the input headers.
			while(lineTokens.length < (inputHeaders.length - headerOffsetCount)) {
				// This can be resolved in a future update by just filling in the blanks.
				// Ideally new rows should be determined by whether token length matches or overflows.
				if (linesRead.isEmpty()) {
					System.out.println("  > Line read remains incomplete at EOF.");
					break main;
				}
				// Merge with the next (originally previous) line and regenerate the tokens.
				lineRead = String.format("%s\n%s", linesRead.pop(), lineRead);
				lineTokens = lineRead.split(CSVUtils.SPLITTING_REGEX, -1);
			}
			
			// Quotation marks should be present to prevent tokens becoming split up.
			// If this is not the case then the token count will mismatch with the header count.
			if (lineTokens.length > (inputHeaders.length - headerOffsetCount)) {
				System.out.println("  > Token count has become unsynchronised.");
				break main;
			}
			
			// Stack the merged line for later use
			this.linesRead.add(lineRead);
		}
		
		// Finalisation.
		this.name = name;
		this.resetLines();
	}
	
	/**
	 * Retrieves the specified token from the current line.
	 * 
	 * @param header Header identifying the token.
	 * 
	 * @return Value of the token, or an empty string.
	 */
	public final String getToken(String header) {
		int index = this.headerIndices.getOrDefault(header, -1);
		if (!this.isEmpty() && this.currentTokens != null && index >= 0) {
			return this.currentTokens[index].replaceAll("\"", "").strip();
		}
		return "";
	}
	
	/**
	 * Checks whether there are more lines to read.
	 * 
	 * @return Result of the check; true or false. 
	 */
	public final boolean isEmpty() {
		return this.linesRead.size() <= this.currentIndex;
	}
	
	/**
	 * Sets the next line as the current line.
	 */
	public final void nextLine() {
		this.setLine(++this.currentIndex);
	}
	
	/**
	 * Sets the previous line as the current line.
	 */
	public final void previousLine() {
		this.setLine(--this.currentIndex);
	}
	
	/**
	 * Resets the current line back to the first.
	 */
	public final void resetLines() {
		this.setLine(0);
	}
	
	/**
	 * Sets the current line for value generation.
	 * 
	 * @param index Index to identify the line.
	 */
	private final void setLine(int index) {
		this.currentIndex = index < 0 ? 0 : index;
		if (this.isEmpty()) {
			this.currentLine = null;
			this.currentTokens = null;
		} else {
			this.currentLine = this.linesRead.get(this.currentIndex);
			this.currentTokens = this.currentLine.split(CSVUtils.SPLITTING_REGEX, -1);
		}
	}
	
}
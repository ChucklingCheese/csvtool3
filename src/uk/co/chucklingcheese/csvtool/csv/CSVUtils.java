package uk.co.chucklingcheese.csvtool.csv;

import java.util.Stack;

import uk.co.chucklingcheese.csvtool.csv.order.ASotHSParser;
import uk.co.chucklingcheese.csvtool.csv.order.BBoxParser;
import uk.co.chucklingcheese.csvtool.csv.order.EbayParser;
import uk.co.chucklingcheese.csvtool.csv.order.EtsyParser;
import uk.co.chucklingcheese.csvtool.csv.order.OnBuyParser;
import uk.co.chucklingcheese.csvtool.csv.order.ShopifyParser;
import uk.co.chucklingcheese.csvtool.csv.order.TFMParser;
import uk.co.chucklingcheese.csvtool.csv.order.UDSParser;
import uk.co.chucklingcheese.csvtool.csv.order.WowcherParser;

/**
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 3.0.0
 * @version 3.0.0
 * 
 */
public class CSVUtils {
	
	/**
	 * Regular expression used to split up comma-separated values.
	 * This will also take into account values within quotation marks.
	 */
	public static final String SPLITTING_REGEX = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
	
	/**
	 * Compares two sets of headers and returns the result.
	 * 
	 * @param expectedHeaders Headers which we are expecting to see.
	 * @param inputHeaders Headers retrieved from an input file.
	 * 
	 * @return Whether all expected headers were found; true or false.
	 */
	private static boolean compareHeaders(String[] expectedHeaders, String[] inputHeaders) {
		int matches = 0;
		for (int i = 0; i < expectedHeaders.length; i++) {
			for (int j = 0; j < inputHeaders.length; j++) {
				if (inputHeaders[j].replaceAll("\"", "").equals(expectedHeaders[i])) {
					matches++;
				}
			}
		}
		return matches == expectedHeaders.length;
	}
	
	/**
	 * Creates a parser based on the input headers.
	 * 
	 * @param inputHeaders Headers retrieved from an input file.
	 * @param linesRead Lines retrieved from an input file.
	 * 
	 * @return A valid parser, or null.
	 */
	public static CSVParser getParser(String[] inputHeaders, Stack<String> linesRead) {
		// Determine whether this is an order export.
		CSVParser parser = getOrderParser(inputHeaders, linesRead);
		if (parser != null) {
			return parser;
		}
		
		// Other CSV types will be checked when supported.
		
		// Unknown.
		return null;
	}
	
	/**
	 * Creates an order parser based on the input headers.
	 * 
	 * @param inputHeaders Headers retrieved from the input file.
	 * @param linesRead Lines retrieved from an input file.
	 * 
	 * @return A valid order export parser, or null.
	 */
	private static CSVOrderParser getOrderParser(String[] inputHeaders, Stack<String> linesRead) {
		// As Seen on the High Street.
		if (compareHeaders(ASotHSParser.PLATFORM_HEADERS, inputHeaders)) {
			return new ASotHSParser(inputHeaders, linesRead);
		// BoroughBox.
		} else if (compareHeaders(BBoxParser.PLATFORM_HEADERS, inputHeaders)) {
			return new BBoxParser(inputHeaders, linesRead);
		// eBay.
		} else if (compareHeaders(EbayParser.PLATFORM_HEADERS, inputHeaders)) {
			return new EbayParser(inputHeaders, linesRead);
		// Etsy.
		} else if (compareHeaders(EtsyParser.PLATFORM_HEADERS, inputHeaders)) {
			return new EtsyParser(inputHeaders, linesRead);
		// OnBuy.
		} else if (compareHeaders(OnBuyParser.PLATFORM_HEADERS, inputHeaders)) {
			return new OnBuyParser(inputHeaders, linesRead);
		// Shopify.
		} else if (compareHeaders(ShopifyParser.PLATFORM_HEADERS, inputHeaders)) {
			return new ShopifyParser(inputHeaders, linesRead);
		// TheFoodMarket.
		} else if (compareHeaders(TFMParser.PLATFORM_HEADERS, inputHeaders)) {
			return new TFMParser(inputHeaders, linesRead);
		// uDropShip.
		} else if (compareHeaders(UDSParser.PLATFORM_HEADERS, inputHeaders)) {
			return new UDSParser(inputHeaders, linesRead);
		// Wowcher.
		} else if (compareHeaders(WowcherParser.PLATFORM_HEADERS, inputHeaders)) {
			return new WowcherParser(inputHeaders, linesRead);
		}
		return null;
	}
	
}